#!/usr/bin/env bash

export JAR="holy-service-0.0.1-SNAPSHOT.jar"
cd unpack

cd BOOT-INF/classes
export LIBPATH=`find ../../BOOT-INF/lib | tr '\n' ':'`
export CP=.:$LIBPATH

# This would run it here... (as an exploded jar)
#java -classpath $CP com.example.demo.DemoApplication

# Our feature being on the classpath is what triggers it
export CP=$CP:../../../../../target/holy-service-0.0.1-SNAPSHOT.jar

printf "\n\nCompile\n"
native-image \
  -Dio.netty.noUnsafe=true \
  --no-server \
  -H:Name=demo \
  -H:+ReportExceptionStackTraces \
  --initialize-at-build-time=org.springframework.util.unit.DataSize,org.springframework.util.Assert,org.slf4j.impl.StaticLoggerBinder,ch.qos.logback.classic.util.ContextSelectorStaticBinder,ch.qos.logback.classic.LoggerContext,ch.qos.logback.classic.Logger,ch.qos.logback.core.spi,ch.qos.logback.classic,ch.qos.logback.core,org.springframework.core,org.springframework.util,org.apache.commons.logging,org.apache.logging.slf4j,org.apache.logging.slf4j.SLF4JLoggerContext,org.apache.logging.log4j.spi.AbstractLogger,org.apache.commons,org.apache.commons.logging,org.apache.logging.log4j.spi.LoggerRegistry,org.apache.logging.log4j.spi.LoggerRegistry$ConcurrentMapFactory \
  --no-fallback \
  --allow-incomplete-classpath \
  --report-unsupported-elements-at-runtime \
  -cp $CP io.payworks.labs.graalvm.poc.holyservice.HolyServiceApplicationKt

# -DremoveUnusedAutoconfig=true \
mv demo ../../..

printf "\n\nCompiled app (demo)\n"
cd ../../..
time ./demo
