package io.payworks.labs.graalvm.poc.holyservice

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication(proxyBeanMethods = false)
class HolyServiceApplication

fun main(args: Array<String>) {
	runApplication<HolyServiceApplication>(*args)
}
