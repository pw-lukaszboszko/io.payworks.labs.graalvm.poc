FROM oracle/graalvm-ce:19.0.2

RUN gu install native-image
ARG JAR_FILE
ADD target/${JAR_FILE} /app.jar

EXPOSE 8080

CMD ["sh", "-c", "java -jar /app.jar"]