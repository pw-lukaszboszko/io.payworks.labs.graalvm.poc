#!/usr/bin/env bash
mvn clean install

export JAR="holy-service-0.0.1-SNAPSHOT.jar"
rm demo
printf "Unpacking $JAR"
rm -rf unpack
mkdir unpack
cd unpack
jar -xvf ../target/$JAR >/dev/null 2>&1
cp -R META-INF BOOT-INF/classes

cd ..

./compile-image.sh
